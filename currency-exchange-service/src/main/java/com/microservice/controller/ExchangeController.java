package com.microservice.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.beans.ExchangeValue;
import com.microservice.repo.ExchangeValueRepository;

@RestController
public class ExchangeController {

	@Autowired
	private Environment envi;
	
	@Autowired
	private ExchangeValueRepository repo;
	
	@GetMapping("/exchange-service/from/{from}/to/{to}")
	public ExchangeValue exchange(@PathVariable String from,@PathVariable String to) {
		ExchangeValue value =  repo.findByFromAndTo(from, to);
		value.setPort(Integer.parseInt(envi.getProperty("local.server.port")));
		return value;

	}
}
